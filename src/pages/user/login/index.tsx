import React from 'react';
import { connect } from 'umi';
import type { Dispatch } from 'umi';
import type { StateType } from '@/models/login';
import type { ConnectState } from '@/models/connect';

export type LoginProps = {
  dispatch: Dispatch;
  userLogin: StateType;
  submitting?: boolean;
};
class Login extends React.Component<LoginProps>{
  componentDidMount(){
    const { dispatch } = this.props;
    dispatch({
      type: 'login/login'
    });
  }
  render() {
    const { children } = this.props;
    return children;
  }
};

export default connect(({ login, loading }: ConnectState) => ({
  userLogin: login,
  submitting: loading.effects['login/login'],
}))(Login);