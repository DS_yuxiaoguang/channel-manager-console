import React from 'react';
import { connect } from 'umi';
import type { ConnectProps } from 'umi';
import type { CurrentUser } from '@/models/user';
import type { ConnectState } from '@/models/connect';

type SecurityLayoutProps = {
  loading?: boolean;
  currentUser?: CurrentUser;
} & ConnectProps;

type SecurityLayoutState = {
  isReady: boolean;
};

class DTransform extends React.Component<SecurityLayoutProps, SecurityLayoutState>{
  componentDidMount(){

  }
}

export default connect(({ login, loading }: ConnectState) => ({
  userLogin: login,
  submitting: loading.effects['login/login'],
}))(DTransform);