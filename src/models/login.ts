import type { Reducer, Effect } from 'umi';
import { history } from 'umi';
import { message } from 'antd';

import { getTokenByAuth } from '@/services/user';
import { setAuthority } from '@/utils/authority';
import { getPageQuery, setAccessToken } from '@/utils/utils';

export type StateType = {
  status?: 'ok' | 'error';
  type?: string;
  currentAuthority?: 'user' | 'guest' | 'admin';
};

export type LoginModelType = {
  namespace: string;
  state: StateType;
  effects: {
    login: Effect;
    logout: Effect;
  };
  reducers: {
    changeLoginStatus: Reducer<StateType>;
  };
};

const Model: LoginModelType = {
  namespace: 'login',
  state: {
    status: undefined,
  },
  effects: {
    *login({}, { call, put }) {      
      let { code: authCode, redirect_uri: forwardURL }: any = getPageQuery() as { forwardURL: string };
      const response = yield call(getTokenByAuth, {authCode, forwardURL});
      yield put({
        type: 'changeLoginStatus',
        payload: {status: 'ok', currentAuthority: 'admin', accessToken: response?.access_token}
      });
      // Login successfully
      if (response?.access_token) {
        const urlParams = new URL(window.location.href);
        message.success('🎉 🎉 🎉  登录成功！');
        if (forwardURL && forwardURL !== 'undefined') {
          const redirectUrlParams = new URL(forwardURL);
          if (redirectUrlParams.origin === urlParams.origin) {
            forwardURL = forwardURL.substr(urlParams.origin.length);
            if (forwardURL.match(/^\/.*#/)) {
              forwardURL = forwardURL.substr(forwardURL.indexOf('#') + 1);
            }
          } else {
            window.location.href = '/';
            return;
          }
        }
        history.replace(forwardURL || '/');
      }
    },
    logout() {
      const { redirect } = getPageQuery();
      setAccessToken('');
      //TODO:// Note: There may be security issues, please note
      if (window.location.pathname !== '/transform' && !redirect) {
        location.href = `https://id.derbysoftsec.com/nonauth/login?appId=ANeRxGE2kwDZ&forwardURL=${window.location.href}`;
        return;
      }
      location.href = '/';
    },
  },
  reducers: {
    changeLoginStatus(state, { payload }) {
      setAccessToken(payload.accessToken)
      setAuthority(payload.currentAuthority);
      return {
        ...state,
        status: payload.status
      };
    },
  },
};

export default Model;
