import { parse } from 'querystring';
const Base64 = require('js-base64').Base64;

/* eslint no-useless-escape:0 import/prefer-default-export:0 */
const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export const isUrl = (path: string): boolean => reg.test(path);

export const isAntDesignPro = (): boolean => {
  if (ANT_DESIGN_PRO_ONLY_DO_NOT_USE_IN_YOUR_PRODUCTION === 'site') {
    return true;
  }
  return window.location.hostname === 'preview.pro.ant.design';
};

// 给官方演示站点用，用于关闭真实开发环境不需要使用的特性
export const isAntDesignProOrDev = (): boolean => {
  const { NODE_ENV } = process.env;
  if (NODE_ENV === 'development') {
    return true;
  }
  return isAntDesignPro();
};

export const getPageQuery = () => parse(window.location.href.split('?')[1]);

export function getAccessToken() {
  let token = localStorage ? localStorage.getItem('accessToken') : '';
  return token;
}

export function setAccessToken(token: string): void {
  localStorage.setItem('accessToken', JSON.stringify(token));
}

export const parseJWTToken = (token: string, options: object) => {
  if(!token) return {};
  let _token = token.slice(token.indexOf('eyJ'));
  let subToken = (JSON.parse(Base64.decode(_token.split('.')[1])) || {});
  return subToken;
};
