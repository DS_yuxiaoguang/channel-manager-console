import request from '@/utils/request';
import { getAccessToken } from '@/utils/utils';

export type LoginParamsType = {
  authCode: string;
  forwardURL: string;
};

export type HeadersType = {
  authorization: any;
  method: string
};

export async function query(): Promise<any> {
  return request('/api/users');
}

export async function queryCurrent(): Promise<any> {
  const headers:HeadersType = {method: 'get', 'authorization': getAccessToken()}
  request.extendOptions({ headers});
  return request('/channel-manager/user/info');
}

export async function getTokenByAuth({authCode, forwardURL}: LoginParamsType) : Promise<any> {
  return request(`/channel-manager/user/getTokenByAuth?authCode=${authCode}&forwardURL=${forwardURL}`);
}
