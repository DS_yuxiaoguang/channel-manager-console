class AppBootHook {
	constructor(app) {
	  this.app = app;
	}
  
	configWillLoad() {
	  console.log('configWillLoad-----')		
	  const db = require('./app/db/index')
	  db();
	}
  
	// async didLoad() {
	// 	console.log('didLoad-----')
	// }
  
	// async willReady() {
	// 	console.log('willReady-----')
	// }
  
	// async didReady() {
    //     console.log('didReady-----')
	// }
  
	// async serverDidReady() {		
	// 	console.log('serverDidReady-----')
	// }
  }
  
  module.exports = AppBootHook;