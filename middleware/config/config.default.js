exports.keys = '2020-12-24-yuxiaoguang';
exports.view = {
    defaultViewEngine: 'nunjucks',
    mapping: {
        '.tpl': 'nunjucks'
    }
}
exports.news = {
    pageSize: 20,
    serviceUrl: 'https://hacker-news.firebaseio.com/v0'
}
exports.middleware = [
	'robot'
];
exports.robot = {
	ua: [
		/Baiduspider/i
	]
}
exports.security = {
    csrf: false
};
