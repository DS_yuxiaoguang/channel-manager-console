const Controller = require('egg').Controller;
class RoleController extends Controller{
    async add(){        
        const ctx = this.ctx;
        const {roleService} = ctx.service;
        const {roleName, status} = ctx.request.body;
        const roleRes = await roleService.add(roleName, status);
        this.ctx.body = roleRes
    }
    async get(){        
        const ctx = this.ctx;
        const {roleService} = ctx.service;
        const {roleName, status} = ctx.request.body;
        const roleRes = await roleService.get(roleName);
        this.ctx.body = roleRes
    }
}

module.exports = RoleController;