const Controller = require('egg').Controller;
const Constants = {
	APP_ID: 'ANeRxGE2kwDZ',
	GRANT_TYPE: 'authorization_code',
	SESSION_TYPE: 'jwt',
	SSOService: 'https://id-owl.derbysoftsec.com/owl'

};
class HomeController extends Controller {
	async getTokenByAuth(){
		const entireToken = await this.getEntireTokenFromDerbySoftID();		
		// const userProfile = await this.getUserProfileFromDerbySoftID(entireToken);
		this.ctx.body = entireToken;
	}
	async getEntireTokenFromDerbySoftID(){
		const {helper, query: {authCode, forwardURL}} = this.ctx;
		const {APP_ID, GRANT_TYPE, SESSION_TYPE, SSOService }= Constants;
		const requestParams = [
			'client_id='+ APP_ID,
			'grant_type=' + GRANT_TYPE,
			'session_type=' + SESSION_TYPE,
			'redirect_uri=' +  forwardURL,
			'code='+ authCode,
			'state='+ Math.random() * 1000
		];
		const entireToken = await helper.getURL([SSOService, '/oauth/token?', requestParams.join('&')].join(''));
		if(entireToken.error) return entireToken.error;
		return entireToken;
	}
	async getUserProfileFromDerbySoftID(){
		const {helper, headers} = this.ctx;
		const {SSOService }= Constants;
		const {authorization: accessToken} = headers;
		const {sub: userId} = helper.parseJWTToken(accessToken);
		const userProfile = await helper.getJSONRPC([SSOService, '/open.rpc'].join(''), 'getUserInfo', [userId], helper.generateAuthorization(accessToken));
		if(userProfile.error) return userProfile.error;
		this.ctx.body = userProfile.result;
	}
}

module.exports = HomeController;
