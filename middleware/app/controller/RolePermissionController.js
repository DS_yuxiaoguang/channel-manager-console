const Controller = require('egg').Controller;
class RolePermissionController extends Controller{
    async add(){        
        const ctx = this.ctx;
        const {rolePermissionService} = ctx.service;
        const {roleId, permissionId, status} = ctx.request.body;
        const rolePermissionRes = await rolePermissionService.add(roleId, permissionId, status);
        this.ctx.body = rolePermissionRes
    }
    async get(){        
        const ctx = this.ctx;
        const {rolePermissionService} = ctx.service;
        const {roleId, permissionId, status} = ctx.request.body;
        const rolePermissionRes = await rolePermissionService.get(roleId, permissionId, status);
        this.ctx.body = rolePermissionRes
    }
}

module.exports = RolePermissionController;