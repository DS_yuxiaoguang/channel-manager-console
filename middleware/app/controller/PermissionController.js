const Controller = require('egg').Controller;
class PermissionController extends Controller{
    async add(){        
        const ctx = this.ctx;
        const {permissionService} = ctx.service;
        const {title, url} = ctx.request.body;
        const permissionRes = await permissionService.add(title, url);
        this.ctx.body = permissionRes
    }
    async get(){        
        const ctx = this.ctx;
        const {permissionService} = ctx.service;
        const {permissionId} = ctx.request.body;
        const permissionRes = await permissionService.get(permissionId);
        this.ctx.body = permissionRes
    }
}

module.exports = PermissionController;