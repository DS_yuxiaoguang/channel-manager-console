const Controller = require('egg').Controller;
class UserGroupController extends Controller{
    async add(){        
        const ctx = this.ctx;
        const {userGroupService} = ctx.service;
        const {groupId, groupName, groupInviteUrl, masterHotelId} = ctx.request.body;
        const userGroupRes = await userGroupService.add(groupId, groupName, groupInviteUrl, masterHotelId);
        this.ctx.body = userGroupRes;
    }
    async get(){        
        const ctx = this.ctx;
        const {userGroupService} = ctx.service;
        const {groupId} = ctx.request.body;
        const userGroupRes = await userGroupService.get(groupId);
        this.ctx.body = userGroupRes;
    }
}

module.exports = UserGroupController;