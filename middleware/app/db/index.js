const mongoose = require('mongoose');
const config = require('./config');

require('./schema/user');
require('./schema/userGroup');
require('./schema/role');
require('./schema/permission');
require('./schema/rolePermission');

const database = () => {
  mongoose.set('debug', false);
  mongoose.connect(config.dbPath, {useNewUrlParser:true}, (err) => {
    if(err) {
      console.log("Mongoose connect error: ", err);
    }
  });

  mongoose.connection.on('disconnected', () => {
    mongoose.connect(config.dbPath);
    console.log('MongoDB is disconnected');
  });

  mongoose.connection.on('error', err => {
    console.log('MongoDB connect error: ', err);
  });

  mongoose.connection.on('open', async () => {
    console.log('Connected to MongoDB successfully! ', config.dbPath);
  })
}

module.exports = database;
