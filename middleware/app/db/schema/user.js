const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {Types: {Number, String, Boolean, ObjectId}} = Schema;
const [PENDING, ACTIVED, DEACTIVED] = ['PENDING', 'ACTIVED', 'DEACTIVED'];
const userSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true
    },
    roleId: {
      type: Number,
      unique: true,
      required: true
    },
    userName: {
      type: String,
      required: false
    },
    groupId: {
      type: Number,
      required: true
    },
    status: {
      type: String,
      required: true,
      default: PENDING,
      enum: [PENDING, ACTIVED, DEACTIVED]
    },
  },
  {
    timestamps: true
  }
);

userSchema.pre('save', function (next) {
  next();
});

mongoose.model('User', userSchema);