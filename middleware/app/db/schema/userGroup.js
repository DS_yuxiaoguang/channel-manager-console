const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {Types: {Number, String, Boolean, ObjectId}} = Schema;
const userGroupSchema = new Schema(
  {
    groupId: {
      type: String,
      required: true,
      unique: true
    },
    groupName: {
      type: String,
      required: true
    },
    groupInviteUrl: {
      type: String,
      required: true
    },
    masterHotelId: {
      type: String,
      required: false,
      unique: true
    },
    accessToken: {
      type: String,
      required: false
    },
  },
  {
    timestamps: true
  }
);

userGroupSchema.pre('save', function (next) {
  next();
});

mongoose.model('UserGroup', userGroupSchema);
