const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {Types: {Number, String, Boolean, ObjectId}} = Schema;
const [PENDING, ACTIVED, DEACTIVED] = ['PENDING', 'ACTIVED', 'DEACTIVED'];
const rolePermissionSchema = new Schema(
  {
    roleId: {
      type: Number,
      required: true
    },
    permissionId: {
      type: Number,
      required: true
    },
    status: {
      type: String,
      required: true,
      enum: [PENDING, ACTIVED, DEACTIVED]
    },
  },
  {
    timestamps: true
  }
);

rolePermissionSchema.pre('save', function (next) {
  next();
});

mongoose.model('RolePermission', rolePermissionSchema);
