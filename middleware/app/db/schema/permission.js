const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Counter = require('./counter');
const {Types: {Number, String, Boolean, ObjectId}} = Schema;
const [PENDING, ACTIVED, DEACTIVED] = ['PENDING', 'ACTIVED', 'DEACTIVED'];
const permissionSchema = new Schema(
  {
    permissionId: {
      type: Number,
      unique: true
    },
    title: {
      type: String,
      required: false
    },
    url: {
      type: String,
      required: true,
      unique: true
    }
  },
  {
    timestamps: true
  }
);

permissionSchema.pre('save', function (next) {
  let doc = this;
  Counter.findByIdAndUpdate({ _id: 'permissionIdSeqGenerator' }, { $inc: { seq: 1 } }, { new: true, upsert: true }, function (error, counter) {
      if (error) return next(error);
      doc.permissionId = counter.seq;
      next();
  });
});

mongoose.model('Permission', permissionSchema);
