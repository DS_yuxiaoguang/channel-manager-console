const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Counter = require('./counter');
const {Types: {Number, String, Boolean, ObjectId}} = Schema;
const [PENDING, ACTIVED, DEACTIVED] = ['PENDING', 'ACTIVED', 'DEACTIVED'];
const roleSchema = new Schema(
  {
    roleId: {
      type: Number,
      unique: true
    },
    roleName: {
      type: String,
      required: true
    },  
    status: {
      type: String,
      required: false,
      default: PENDING, 
      enum: [PENDING, ACTIVED, DEACTIVED]
    },
    comments: {
      type: String,
      required: false
    },  
  },
  {
    timestamps: true
  }
);

roleSchema.pre('save', function (next) {  
  let doc = this;
  Counter.findByIdAndUpdate({ _id: 'roleIdSeqGenerator' }, { $inc: { seq: 1 } }, { new: true, upsert: true }, (error, counter) => {
      if (error) return next(error);
      doc.roleId = counter.seq;
      next();
  });
});

mongoose.model('Role', roleSchema);
