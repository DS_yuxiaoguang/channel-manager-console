const dbConfig = {
  dbName: 'go-booking',
  host: 'localhost',
  port: '27017',
  dbPath: 'mongodb://localhost:27017/connection-manager'
};

module.exports = dbConfig;

