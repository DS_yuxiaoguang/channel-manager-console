const modelName = 'Permission';
const Service = require('egg').Service;
const mongoose = require('mongoose');
const PermissionModel = mongoose.model(modelName);
const path = require('path');
class PermissionService extends Service{
  constructor(permission = {}) {
    super(permission);
    this.title = permission.title;
    this.url = permission.url;
    this.permissionId = permission.permissionId;
  }

  async add(title, url, options = {}){
    let condition = {};
    condition = title ? Object.assign(condition, {title}) : condition;
    condition = url ? Object.assign(condition, {url}) : condition;
    const doc = {title, url};
    const permissionRes = await PermissionModel.findOne(condition);
    if(permissionRes && permissionRes.url && permissionRes.title) return permissionRes;
    return await PermissionModel.create(doc);
  }

  async get(permissionId, title, url, options = {}){
    let condition = {};
    condition = Object.assign(condition, {permissionId});
    condition = title ? Object.assign(condition, {title}) : condition;
    condition = url ? Object.assign(condition, {url}) : condition;
    return await PermissionModel.findOne(condition);
  }

  async getAll(options = {}){
    return await {};
  }

  async update(permissionId, title, url,  options = {}){
    let condition = {permissionId};
    let doc = {};
    doc = Object.assign(doc, {title});
    doc = Object.assign(doc, {url});
    return await PermissionModel.findOneAndUpdate(condition, doc, {new: true});
  }

  async delete(permissionId, options = {}){
    const condition = {permissionId};
    return await PermissionModel.deleteOne(condition);
  }
}

module.exports = PermissionService;