const modelName = 'UserGroup';
const Service = require('egg').Service;
const mongoose = require('mongoose');
const UserGroupModel = mongoose.model(modelName);
const path = require('path');
class UserGroupService extends Service{
  constructor(userGroup = {}) {
    super(userGroup);
    this.groupId = userGroup.groupId;
    this.groupName = userGroup.groupName;
    this.groupInviteUrl = userGroup.groupInviteUrl;
    this.masterHotelId = userGroup.masterHotelId;
  }

  async add(groupId, groupName, groupInviteUrl, masterHotelId, options = {}){
    let condition = {groupId};
    let doc = {groupId, groupName, groupInviteUrl};
    doc = masterHotelId ? Object.assign(doc, {masterHotelId}) : doc;
    const groupRes = await UserGroupModel.findOne(condition);
    if(groupRes && groupRes.groupId) return groupRes;
    return await UserGroupModel.create(doc);
  }

  async get(groupId, groupName, masterHotelId, options = {}){
    let condition = {groupId};
    condition = groupName ? Object.assign(condition, {groupName}) : condition;
    condition = masterHotelId ? Object.assign(condition, {masterHotelId}) : condition;
    return await UserGroupModel.findOne(condition);
  }

  async getAll(options = {}){
    return await {};
  }

  async update(groupId, groupName, groupInviteUrl, masterHotelId, options = {}){
    let condition = {groupId};
    let doc = {};
    condition = Object.assign(condition, {groupId});
    condition = Object.assign(condition, {masterHotelId});
    doc = Object.assign(doc, {groupName});
    doc = Object.assign(doc, {groupInviteUrl});
    return await UserGroupModel.findOneAndUpdate(condition, doc, {new: true});
  }

  async delete(groupId, options){
    const condition = {groupId};
    return await UserGroupModel.deleteOne(condition);
  }
}

module.exports = UserGroupService;