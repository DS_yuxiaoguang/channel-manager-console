const modelName = 'RolePermission';
const Service = require('egg').Service;
const mongoose = require('mongoose');
const RolePermissionModel = mongoose.model(modelName);
const path = require('path');
class RolePermissionService extends Service{
  constructor(rolePermission = {}) {
    super(rolePermission);
    this.roleId = rolePermission.roleId;
    this.permissionId = rolePermission.permissionId;
    this.status = rolePermission.status;
  }

  async add(roleId, permissionId, status, options = {}){
    let condition = {roleId, permissionId};
    const doc = {roleId, permissionId, status};
    const rolePermissionRes = await RolePermissionModel.findOne(condition);
    if(rolePermissionRes && rolePermissionRes.roleId && rolePermissionRes.permissionId) return rolePermissionRes;
    return await RolePermissionModel.create(doc);
  }

  async get(roleId, permissionId, status, options = {}){
    let condition = {roleId, permissionId};
    condition = Object.assign(condition, {status});
    return await RolePermissionModel.findOne(condition);
  }

  async getAll(callback, options = {}){
    return await {};
  }

  async update(roleId, permissionId, status, options = {}){
    let condition = {roleId, permissionId};
    let doc = {status};
    return await RolePermissionModel.findOneAndUpdate(condition, doc, {new: true});
  }

  async delete(roleId, permissionId, options){
    const condition = {roleId, permissionId};
    return await RolePermissionModel.deleteOne(condition);
  }
}

module.exports = RolePermissionService;