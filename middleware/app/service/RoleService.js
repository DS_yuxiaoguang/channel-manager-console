const modelName = 'Role';
const Service = require('egg').Service;
const mongoose = require('mongoose');
const RoleModel = mongoose.model(modelName);
class RoleService extends Service{
  constructor(role = {}) {
    super(role);
    this.roleId = role.roleId;
    this.roleName = role.roleName;
    this.status = role.status;
    this.comments = role.comments;
  }

  async add(roleName, status, comments, options = {}){
    let condition = {};
    condition = Object.assign(condition, {roleName});
    const doc = {roleName, status, comments};
    const roleRes = await RoleModel.findOne(condition);
    if(roleRes && roleRes.email) return roleRes;
    return await RoleModel.create(doc);
  }

  async get(roleName, status, callback, options = {}){
    let condition = {roleName};
    condition = status ? Object.assign(condition, {status}) : condition;
    return await RoleModel.findOne(condition);
  }

  async getAll(options = {}){
    return await {};
  }

  async update(options = {}){   
    const {roleId, roleName, status, comments} = this;
    let condition = {roleId};
    let doc = {};
    doc = Object.assign(doc, {roleName});
    doc = Object.assign(doc, {status});
    doc = Object.assign(doc, {comments});
    return await RoleModel.findOneAndUpdate(condition, doc, {new: true});
  }

  async delete(roleId, options = {}){
    const condition = {roleId};
    return await RoleModel.deleteOne(condition);
  }
}

module.exports = RoleService;