const modelName = 'User';
const Service = require('egg').Service;
const mongoose = require('mongoose');
const UserModel = mongoose.model(modelName);
const path = require('path');
class UserService extends Service {
  constructor(user = {}) {
    super(user);
    this.email = user.email;
    this.roleId = user.roleId;
    this.groupId = user.groupId;
    this.userName = user.userName;
    this.status = user.status;
  }

  async add(email, roleId, groupId, userName, status, options = {}){
    let condition = {};
    condition = Object.assign(condition, {email});
    const doc = {email, roleId, groupId, userName, status};
    const userRes = await UserModel.findOne(condition);
    if(userRes && userRes.email) return userRes;
    return await UserModel.create(doc);
  }

  async get(email, options = {}){
    let condition = {};
    condition = Object.assign(condition, {email});
    return await UserModel.findOne(condition);
  }

  async getAll(options = {}){
    return await {};
  }

  async update(email, roleId, userName, status, options = {}){
    let condition = {email};
    let doc = {};
    doc = Object.assign(doc, {roleId});
    doc = Object.assign(doc, {userName});
    doc = Object.assign(doc, {status});
    return await UserModel.findOneAndUpdate(condition, doc, {new: true})
  }

  async delete(email, options = {}){
    const condition = {email};
    return await UserModel.deleteOne(condition);
  }
}

module.exports = UserService;
