const moment = require('moment');
const request = require('request');
const Base64 = require('js-base64').Base64;
exports.relativeTime = time => moment(new Date(time * 1000)).fromNow();
exports.getURL = async (url, options = {headers: {}}) => {
    let _options = {
        url,
        method: 'GET',
        encoding: options.encoding || null,
        timeout: options.timeout || 300000,
        cookie: options.cookie || null
    };
    if (options.headers) Object.assign(_options, {headers: Object.assign({'Authorization': options.headers['authorization']})})
    return new Promise((resolve, reject) => request(_options, (err, response, body) => {
      if (err) {
      reject(err);
      } else {
      resolve(body);
      }
    }));
};
exports.getJSONRPC = async (url, method, params, options = {}) => {
    options = Object.assign({headers: {}}, options);
    let body;
    if (method) {
      body = JSON.stringify({"method": method, "params": params});
    }
    if (!method.length) {
      body = JSON.stringify(params);
    }
  
    let headers = {};
    headers['Content-Type'] = 'application/json';
    headers['Authorization'] = options.headers['authorization'];
    options = {
      url: url,
      method: 'POST',
      headers: headers,
      body: body,
      timeout: options.timeout || 300000
    };

    return new Promise((resolve, reject) => request(options, (err, response, res) => {
      if (err) {
        reject(err);
        } else {
          res = typeof res == 'string' ? JSON.parse(res) : res;
          resolve(res);
        }
    }))
    
};
exports.parseJWTToken = (token) => {
    if(!token) return {};
    let _token = token.slice(token.indexOf('eyJ'));
    let subToken = (JSON.parse(Base64.decode(_token.split('.')[1])) || {});
    return subToken;
}
exports.generateAuthorization = (accessToken) => {
    return {headers: {"authorization": "Bearer " + accessToken}};
}
exports.parseJWTToken = (token, options) => {
  if(!token) return {};
  let _token = token.slice(token.indexOf('eyJ'));
  let subToken = (JSON.parse(Base64.decode(_token.split('.')[1])) || {});
  return subToken;
};
