module.exports = app => {
	const {router, controller: {homeController, userController, userGroupController, roleController, permissionController, rolePermissionController}} = app;
	router.get('/channel-manager/user/getTokenByAuth', homeController.getTokenByAuth);
	router.get('/channel-manager/user/info', homeController.getUserProfileFromDerbySoftID);
	
	router.post('/user/add', userController.add);
	router.post('/channel-manager/user/get', userController.get);
	router.post('/userGroup/add', userGroupController.add);
	router.post('/userGroup/get', userGroupController.get);
	router.post('/role/add', roleController.add);
	router.post('/role/get', roleController.get);
	router.post('/permission/add', permissionController.add);
	router.post('/permission/get', permissionController.get);
	router.post('/rolePermission/add', rolePermissionController.add);
	router.post('/rolePermission/get', rolePermissionController.get);
	
	
	// router.get('/home', controller.home);
	// router.get('/user/:id', controller.user.page);
	// router.post('/admin', isAdmin, controller.admin);
	// router.post('/user', isLoginUser, hasAdminPermission, controller.user.create);
	// router.post('/api/v1/comments', controller.v1.comments.create); // app/controller/v1/comments.js
}
